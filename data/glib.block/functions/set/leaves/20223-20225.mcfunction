execute if score @s glib.blockId matches 20223 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=true,waterlogged=true,west=low]
execute if score @s glib.blockId matches 20224 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=true,waterlogged=true,west=tall]
execute if score @s glib.blockId matches 20225 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=none,up=true,waterlogged=false,west=none]
