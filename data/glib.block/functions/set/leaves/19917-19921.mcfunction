execute if score @s glib.blockId matches 19917 run setblock ~ ~ ~ deepslate_tile_wall[east=tall,north=tall,south=tall,up=false,waterlogged=false,west=low]
execute if score @s glib.blockId matches 19918 run setblock ~ ~ ~ deepslate_tile_wall[east=tall,north=tall,south=tall,up=false,waterlogged=false,west=tall]
execute if score @s glib.blockId matches 19919 run setblock ~ ~ ~ deepslate_bricks
execute if score @s glib.blockId matches 19920 run setblock ~ ~ ~ deepslate_brick_stairs[facing=north,half=top,shape=straight,waterlogged=true]
execute if score @s glib.blockId matches 19921 run setblock ~ ~ ~ deepslate_brick_stairs[facing=north,half=top,shape=straight,waterlogged=false]
